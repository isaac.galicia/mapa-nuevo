var wms_layers = [];

        var lyr_MapboxSatellite_0 = new ol.layer.Tile({
            'title': 'MapboxSatellite_0',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: [new ol.Attribution({html: '<a href="https://www.mapbox.com/about/maps">Terms & Feedback</a>'})],
                url: 'https://a.tiles.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.jpg?access_token=pk.eyJ1Ijoib3BlbnN0cmVldG1hcCIsImEiOiJja2w5YWt5bnYwNjZmMnFwZjhtbHk1MnA1In0.eq2aumBK6JuRoIuBMm6Gew'
            })
        });var format_division_polit_1 = new ol.format.GeoJSON();
var features_division_polit_1 = format_division_polit_1.readFeatures(json_division_polit_1, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_division_polit_1 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_division_polit_1.addFeatures(features_division_polit_1);var lyr_division_polit_1 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_division_polit_1, 
                style: style_division_polit_1,
                title: '<img src="styles/legend/division_polit_1.png" /> division_polit'
            });var format_CO2_Instituciones_2 = new ol.format.GeoJSON();
var features_CO2_Instituciones_2 = format_CO2_Instituciones_2.readFeatures(json_CO2_Instituciones_2, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_CO2_Instituciones_2 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_CO2_Instituciones_2.addFeatures(features_CO2_Instituciones_2);var lyr_CO2_Instituciones_2 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_CO2_Instituciones_2, 
                style: style_CO2_Instituciones_2,
    title: 'CO2_Instituciones<br />\
    <img src="styles/legend/CO2_Instituciones_2_0.png" /> Colegio de la Frontera Norte [COLEF]<br />\
    <img src="styles/legend/CO2_Instituciones_2_1.png" /> Comision Federal de Electricidad [CFE]<br />\
    <img src="styles/legend/CO2_Instituciones_2_2.png" /> Facultad de Ingenieria [FI,UNAM]<br />\
    <img src="styles/legend/CO2_Instituciones_2_3.png" /> Facultad de Quimica [FQ, UNAM]<br />\
    <img src="styles/legend/CO2_Instituciones_2_4.png" /> Instituto de  [IG, UNAM]<br />\
    <img src="styles/legend/CO2_Instituciones_2_5.png" /> Instituto de Electricidad y Energias Limpias [INEEL]<br />\
    <img src="styles/legend/CO2_Instituciones_2_6.png" /> Instituto de Ingenieria [II, UNAM]<br />\
    <img src="styles/legend/CO2_Instituciones_2_7.png" /> Instituto de Investigacion en Materiales [IIM, UNAM]<br />\
    <img src="styles/legend/CO2_Instituciones_2_8.png" /> Instituto Mexicano del Petroleo [IMP]<br />\
    <img src="styles/legend/CO2_Instituciones_2_9.png" /> Instituto Politécnico Nacional [IPN]<br />\
    <img src="styles/legend/CO2_Instituciones_2_10.png" /> Petroleos Mexicanos [PEMEX]<br />\
    <img src="styles/legend/CO2_Instituciones_2_11.png" /> Secretaría de Energía [SENER]<br />\
    <img src="styles/legend/CO2_Instituciones_2_12.png" /> Universidad Autónoma de Chapingo [UACh]<br />\
    <img src="styles/legend/CO2_Instituciones_2_13.png" /> Universidad Autónoma de México, Unidad Azcapozalco [UAM]<br />\
    <img src="styles/legend/CO2_Instituciones_2_14.png" /> Universidad Autónoma de Nuevo León [UANL]<br />\
    <img src="styles/legend/CO2_Instituciones_2_15.png" /> Universidad Autónoma del Estado de Morelos <br />\
    <img src="styles/legend/CO2_Instituciones_2_16.png" /> Universidad Politécnica de la Energía [UPE]<br />'
        });

lyr_MapboxSatellite_0.setVisible(true);lyr_division_polit_1.setVisible(true);lyr_CO2_Instituciones_2.setVisible(true);
var layersList = [lyr_MapboxSatellite_0,lyr_division_polit_1,lyr_CO2_Instituciones_2];
lyr_division_polit_1.set('fieldAliases', {'AREA': 'AREA', 'PERIMETER': 'PERIMETER', 'COV_': 'COV_', 'COV_ID': 'COV_ID', 'ESTADO': 'ESTADO', 'CVE_EDO': 'CVE_EDO', });
lyr_CO2_Instituciones_2.set('fieldAliases', {'fid': 'fid', 'Latitude': 'Latitude', 'Longitude': 'Longitude', 'Institución': 'Institución', 'Proyectos de la Institución': 'Proyectos de la Institución', 'Área de Investigación': 'Área de Investigación', 'Tipo': 'Tipo', 'Año de Publicación': 'Año de Publicación', 'Líder': 'Líder', 'Miembros': 'Miembros', 'Proyecto Actual': 'Proyecto Actual', 'Descripción': 'Descripción', 'Website': 'Website', 'Email': 'Email', });
lyr_division_polit_1.set('fieldImages', {'AREA': 'TextEdit', 'PERIMETER': 'TextEdit', 'COV_': 'TextEdit', 'COV_ID': 'TextEdit', 'ESTADO': 'TextEdit', 'CVE_EDO': 'TextEdit', });
lyr_CO2_Instituciones_2.set('fieldImages', {'fid': 'TextEdit', 'Latitude': 'TextEdit', 'Longitude': 'TextEdit', 'Institución': 'TextEdit', 'Proyectos de la Institución': 'TextEdit', 'Área de Investigación': 'TextEdit', 'Tipo': 'TextEdit', 'Año de Publicación': 'TextEdit', 'Líder': 'TextEdit', 'Miembros': 'TextEdit', 'Proyecto Actual': 'TextEdit', 'Descripción': 'TextEdit', 'Website': 'TextEdit', 'Email': 'TextEdit', });
lyr_division_polit_1.set('fieldLabels', {'AREA': 'no label', 'PERIMETER': 'no label', 'COV_': 'no label', 'COV_ID': 'no label', 'ESTADO': 'no label', 'CVE_EDO': 'no label', });
lyr_CO2_Instituciones_2.set('fieldLabels', {'fid': 'header label', 'Latitude': 'header label', 'Longitude': 'header label', 'Institución': 'header label', 'Proyectos de la Institución': 'header label', 'Área de Investigación': 'header label', 'Tipo': 'header label', 'Año de Publicación': 'header label', 'Líder': 'header label', 'Miembros': 'header label', 'Proyecto Actual': 'header label', 'Descripción': 'header label', 'Website': 'header label', 'Email': 'header label', });
lyr_CO2_Instituciones_2.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});